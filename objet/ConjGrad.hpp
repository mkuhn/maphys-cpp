#ifndef CONJGRAD_H
#define CONJGRAD_H


#include <iostream>
#include <Eigen/Dense>


namespace maphys{

  int cg(const Eigen::MatrixXf &A, const Eigen::VectorXf &b, Eigen::VectorXf &X,
	 float eps = 0.01, int Nmax = 50);
  
  class ConjGrad

  {
  public:

    ConjGrad(const Eigen::MatrixXf &A, Eigen::VectorXf &X);

    ConjGrad(const Eigen::MatrixXf &A);
  
    int solve(const Eigen::VectorXf &b);

    Eigen::VectorXf getX() const {return _X;}

    Eigen::VectorXf operator*(const Eigen::VectorXf &b);
  
  private:
  
    Eigen::VectorXf _X;
    const Eigen::MatrixXf _A;
  
  

  };
  
}

#endif
