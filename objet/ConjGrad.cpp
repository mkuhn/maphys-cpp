#include <iostream>
#include <Eigen/Dense>
#include "ConjGrad.hpp"

using namespace Eigen;
using namespace std;


namespace maphys{

int cg(const MatrixXf &A, const VectorXf &b, VectorXf &X, float eps, int Nmax)
{
    VectorXf q;
    VectorXf r = b - A*X;
    VectorXf p = r;
    float alpha;
    float beta;
    float norm_r_old = r.dot(r);
    float norm_r_new;

    for (int i = 0; i < Nmax; i++){
        q = A * p;
        alpha = norm_r_old/p.dot(q);
        X += alpha * p;
        r -= alpha * q ;
        cout << "||r||² = " << r.norm() << endl;
        if (r.norm() < eps){
            return 0;
        }
        norm_r_new = r.dot(r);
        beta = norm_r_new/norm_r_old;
        norm_r_old = norm_r_new;
        p = beta * p + r;
    }
    return 1;
}

ConjGrad::ConjGrad(const MatrixXf &A, VectorXf &X):
    _X(X),
    _A(A)
{
}

ConjGrad::ConjGrad(const MatrixXf &A):
    _X(A.rows()),
    _A(A)
{
}

VectorXf ConjGrad::operator*(const VectorXf &b)
{
    solve(b);
    return _X;
}

int ConjGrad::solve(const VectorXf &b)
{
    return cg(_A, b, _X, 0.01, 50);
}

} // namespace maphys
