#include <iostream>
#include "ConjGrad.hpp"
#include <Eigen/Dense>


using namespace std;
using namespace maphys;
using namespace Eigen;

int main()
{
  VectorXf B(2);
  B(0) = 1;
  B(1) = 1;

  MatrixXf A(2,2);
  A(0,0) = 4;
  A(0,1) = A(1,0) = 1;
  A(1,1) = 3;

  
  //ConjGrad S(A);
  //S.solve(B);
  //cout << "La solution X de AX = B est : X =" << endl << S.getX() << endl;
  cout << "La solution X de AX = B est : X =" << endl << ConjGrad(A)*B << endl;
  
  return 0;
}
