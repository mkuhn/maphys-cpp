#EIGENDIR=$(shell spack location -i eigen)
EIGENDIR=/home/user/spack/opt/spack/linux-x86_64/gcc-4.8/eigen-3.3.2-fnzqdfjb7fl3ranvyifhvticir3wv3ox
ifeq ($(EIGENDIR),)
$(error "Eigen dir is not found" )
endif

CC=clang++
INCLUDES=-I$(EIGENDIR)/include/eigen3
CFLAGS=-Wall -Wextra -std=c++11	$(INCLUDES)
LDFLAGS=
LIBS=
GENERATED_SOURCES=
DEPS=$(wildcard *.dep)
TARGETS=cg creux_cg

SRC_CG = cg.cpp
OBJ_CG=$(SRC_CG:.cpp=.o)

SRC_CREUX_CG = creux_cg.cpp
OBJ_CREUX_CG=$(SRC_CREUX_CG:.cpp=.o)


ifdef DEBUG
CFLAGS+=-O0 -ggdb
LDFLAGS+=-O0 -ggdb
else
CFLAGS+=-O2
LDFLAGS+=-O2
endif

ifneq (,$(findstring clang,$(CC)))
CFLAGS+=-ferror-limit=2
else ifneq (,$(findstring g++,$(CC)))
CFLAGS+=-fmax-errors=2
endif

all: $(TARGETS)

-include $(DEPS)

%.o: %.cpp
	@$(CC) -MM $(CFLAGS) -c -o $*.dep $<
	$(CC) $(CFLAGS) -c -o $@ $<

cg:  $(OBJ_CG)
	$(CC) $^ -o $@ $(LDFLAGS) $(LIBS)

creux_cg:  $(OBJ_CREUX_CG)
	$(CC) $^ -o $@ $(LDFLAGS) $(LIBS)

clean:
	$(RM) $(OBJ_CG) $(OBJ_CREUX_CG)

fullclean: clean
	$(RM) $(TARGETS) $(DEPS) $(GENERATED_SOURCES)
