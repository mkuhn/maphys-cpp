#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>

using namespace std;
using namespace Eigen;

template <typename M, typename V>
V cg(M A, V b, V X, float eps, int Nmax)
{
    V q;
    V r = b - A*X;
    V p = r;
    float alpha;
    float beta;
    float norm_r_old = r.dot(r);
    float norm_r_new;

    for (int i = 0; i < Nmax; i++) {
        q = A * p;
        alpha = norm_r_old/p.dot(q);
        X += alpha * p;
        r -= alpha * q ;
        cout << r.norm() << endl;
        if (r.norm() < eps){
            break ;
        }
        norm_r_new = r.dot(r);
        beta = norm_r_new/norm_r_old;
        norm_r_old = norm_r_new;
        p = beta * p + r;

    }
    return X;
}

int main()
{

    //-----------------Tests-------------------

    int Nmax = 50;
    float eps = 0.01;
    int n = 2;
    (void) n;

    //--------> matrice float et vecteur float<---------------
    MatrixXf A(2,2);
    A(0,0) = 4;
    A(1,0) = 1;
    A(0,1) = 1;
    A(1,1) = 3;

    VectorXf  B(2);
    B(0) = 1;
    B(1) = 2;
    VectorXf X(2);
    X(0) = 2;
    X(1) = 1;


    //-----------> matrice double et vecteur double <---------
    MatrixXd Ad(2,2);
    Ad(0,0) = 4;
    Ad(1,0) = 1;
    Ad(0,1) = 1;
    Ad(1,1) = 3;

    VectorXd  Bd(2);
    Bd(0) = 1;
    Bd(1) = 2;
    VectorXd Xd(2);
    Xd(0) = 2;
    Xd(1) = 1;

    //----------->matrice creuse<------------

    SparseMatrix<double> Ac(3,3);
    Ac.coeffRef(0,0) = 1;
    Ac.coeffRef(1,1) = 0.4;
    Ac.coeffRef(2,2) = 0.25;

    Ac.coeffRef(0,1) = Ac.coeffRef(0,1) = 0;
    Ac.coeffRef(0,2) = Ac.coeffRef(2,0) = 0;
    Ac.coeffRef(1,2) = Ac.coeffRef(2,1) = 0.1;


    SparseVector<double> Bc(3);
    Bc.coeffRef(0) = 0 ;
    Bc.coeffRef(1) = 1 ;
    Bc.coeffRef(2) = 5 ;


    SparseVector<double> Xc(3);
    Xc.coeffRef(0) = 0 ;
    Xc.coeffRef(1) = 0 ;
    Xc.coeffRef(2) = 0 ;
    Xc = cg(Ac,Bc,Xc,eps,Nmax);


    //cout << "Ax" << (A*X) << endl;
    //cout << "Adxd" << (Ad*Xd) << endl;
    cout << "Solution f : " << cg(A,B,X,eps,Nmax) << endl;
    cout << "Solution d : " << cg(Ad,Bd,Xd,eps,Nmax) << endl;
    cout << "Solution c : " << Xc << endl;
    cout << "Solution c-verification: " << ((Ac*Xc) - Bc) << endl;

    return 0;

}
