#ifndef CONJGRAD_H
#define CONJGRAD_H

#include <iostream>

namespace maphys{

template <typename M, typename V>
int cg(const M &A, const V &b, V &x, float eps = 0.01, int Nmax = 50, bool verbose=true)
{
    V q;
    V r = b - A*x;
    V p = r;
    float alpha;
    float beta;
    float norm_r_old = r.dot(r);
    float norm_r_new;

    for (int i = 0; i < Nmax; i++)
    {
        q = A * p;
        alpha = norm_r_old/p.dot(q);
        x += alpha * p;
        r -= alpha * q ;
        if (verbose)
            std::cout << "||r||² = " << r.norm() << std::endl;
        if (r.norm() < eps)
            return 0;
        norm_r_new = r.dot(r);
        beta = norm_r_new/norm_r_old;
        norm_r_old = norm_r_new;
        p = beta * p + r;
    }
    return 1;
}

template <class M, class V>
class ConjGrad
{
public:
    ConjGrad(const M  &A, V &x):
        _x(x),
        _A(A)
    {
    }

    ConjGrad(const M &A):
        _x(A.rows()),
        _A(A)
    {
        _x *= 0;
    }

    int solve(const V &b)
    {
        return cg(_A, b, _x, 0.01, 50);
    }

    V getx() const {return _x;}

    V operator*(const V &b)
    {
        solve(b);
        return _x;
    }

private:
    V _x;
    M _A;

}; // class ConjGrad
}  // namespace maphys

#endif
