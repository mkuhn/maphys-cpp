#include <iostream>
#include "ConjGrad.hpp"
#include <Eigen/Dense>
#include <Eigen/Sparse>


using namespace std;
using namespace maphys;
using namespace Eigen;

int main()
{
  VectorXf b(2);
  b(0) = 1;
  b(1) = 1;

  MatrixXf A(2,2);
  A(0,0) = 4;
  A(0,1) = A(1,0) = 1;
  A(1,1) = 3;

  SparseVector<double> b_sparse(2);
  b_sparse.coeffRef(0) = 1;
  b_sparse.coeffRef(1) = 1;

  SparseMatrix<double> A_sparse(2,2);
  A_sparse.coeffRef(0,0) = 4;
  A_sparse.coeffRef(1,1) = 3;
  A_sparse.coeffRef(0,1) = 1;
  A_sparse.coeffRef(1,0) = 1;



  cout << "La solution x de Ax = b est : x =" << endl << ConjGrad<MatrixXf, VectorXf>(A)*b << endl;
  cout << "La solution x de A_sparse x = b est : x =" << endl;
  cout << ConjGrad<SparseMatrix<double>, SparseVector<double> >(A_sparse)*b_sparse << endl;

  return 0;
}
