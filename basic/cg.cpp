#include <iostream>
#include <Eigen/Dense>

using namespace Eigen;
using namespace std;


MatrixXf generer_sdp(int n)
{
  MatrixXf m = MatrixXf::Random(n,n);
  MatrixXf sdp = m * m.transpose();
  return sdp;
}

VectorXf generer_vector(int n){

  VectorXf V = VectorXf::Random(n);
  return V;
}


VectorXf cg(MatrixXf A, VectorXf b, VectorXf X, float eps, int Nmax)
{
  VectorXf q;
  VectorXf r = b - A*X;
  VectorXf p = r;
  
  float alpha;
  float beta;
  float norm_r_old = r.dot(r);
  float norm_r_new;
  
  int n = 0;

  for (int i = 0; i < Nmax; i++){
    q = A * p; 
    alpha = norm_r_old/p.dot(q);  
    X += alpha * p;  
    r -= alpha * q ;
    cout << r.norm() << endl;
    if (r.norm() < eps){
      break ;
    }
    norm_r_new = r.dot(r);
    beta = norm_r_new/norm_r_old;
    norm_r_old = norm_r_new; 
    p = beta * p + r;
    n++;
      
  }
  return X;
}


int main()
{
  int Nmax = 500;
  float eps = 0.001;
  int n = 3;
  MatrixXf A(2,2);
  A(0,0) = 4;
  A(1,0) = 1;
  A(0,1) = 1;
  A(1,1) = 3;
  
  VectorXf  B(2);
  B(0) = 1;
  B(1) = 2;
  VectorXf X(2);
  X(0) = 2;
  X(1) = 1;

  // test pour le creux

  MatrixXf Ac(4,4);
  Ac(0,0) = 4;
  Ac(1,1) = 5;
  Ac(2,2) = 6;
  Ac(3,3) = 7;

  Ac(0,1) = Ac(1,0) = 0;
  Ac(0,2) = Ac(2,0) = 0;
  Ac(0,3) = Ac(3,0) = 0;
  Ac(1,2) = Ac(2,1) = 0;
  Ac(1,3) = Ac(3,1) = 0;
  Ac(2,3) = Ac(3,2) = 0;
  
  VectorXf  Bc(4);
  Bc(0) = 1;
  Bc(1) = 1;
  Bc(2) = 1;
  Bc(3) = 1;
  VectorXf Xc(4);
  Xc(0) = 0;
  Xc(1) = 0;
  Xc(2) = 0;
  Xc(3) = 0;

  //------->Tests<---------- :

  //MatrixXf A = generer_sdp(n);
  //VectorXf V = generer_vector(n);
  //VectorXf X0 = generer_vector(n);
  //cout << "A =" << endl << A << endl;
  //cout << "V =" << endl << V << endl;
  //cout << "X =" << endl << X << endl;
  //cout << "norm of X =" << endl << V.norm() << endl;
  //cout << "Le produit scalaire de B est :" << endl << B.dot(B) << endl;
  VectorXf solutionc = cg(Ac,Bc,Xc,eps,Nmax);
  cout << "La solution X de AX = B est : X =" << endl << solutionc << endl;
  cout << "Verification" << endl << Ac*solutionc << endl;

}

