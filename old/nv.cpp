#include <iostream>
#include <Eigen/Dense>

using namespace Eigen;
using namespace std;

template<class S> using Vec = Matrix<S, Dynamic, 1>;
template<class S> using Mat = Matrix<S, Dynamic, Dynamic>;


class MaMatrix 
{
  int m;
  int n;
  double *val;

  int print_matrix_size() { return m; }
};

class MatrixEtendue :  public MaMatrix 
{
  
}

template<class S>
Mat<S> generer_sdp(int n){

  Mat<S> m = Mat<S>::Random(n,n);
  Mat<S> sdp = m * m.transpose();
  return sdp;
}

template<class S>
Vec<S> generer_vector(int n)
{
  Vec<S> V = Vec<S>::Random(n);
  return V;
}

operator=()


template<class Matrix, class S>
Vec<S> cg(Mat<S> A, Vec<S> b, Vec<S> X, S eps, int Nmax)
{
  Vec<S> q;
  Vec<S> r = b - A*X;
  Vec<S> p = r;
  
  float alpha;
  float beta;
  float norm_r_old = r.dot(r);
  float norm_r_new;
  
  int n = 0;

  for (int i = 0; i < Nmax; i++){
      q = A * p; 
      alpha = norm_r_old/p.dot(q);  
      X += alpha * p;  
      r -= alpha * q ;
      cout << r.norm() << endl;
      if (r.norm() < eps){
	break ;
      }
      norm_r_new = r.dot(r);
      beta = norm_r_new/norm_r_old;
      norm_r_old = norm_r_new; 
      p = beta * p + r;
      n++;
      
  }
  return X;
}


int main()
{
  int Nmax = 5;
  float eps = 0.01;

  int n = 3;
  Mat<float> A(2,2);
  A(0,0) = 4;
  A(1,0) = 1;
  A(0,1) = 1;
  A(1,1) = 3;
  Vec<float>  B(2);
  B(0) = 1;
  B(1) = 2;
  Vec<float> X(2);
  X(0) = 2;
  X(1) = 1;
 

  //------->Tests<---------- :

  //Mat<S> A = generer_sdp(n);
  //Vec<S> V = generer_vector(n);
  //Vec<S> X0 = generer_vector(n);
  //cout << "A =" << endl << A << endl;
  //cout << "V =" << endl << V << endl;
  //cout << "X =" << endl << X << endl;
  //cout << "norm of X =" << endl << V.norm() << endl;
  //cout << "Le produit scalaire de B est :" << endl << B.dot(B) << endl;
  auto solution = cg(A,B,X,eps,Nmax);
  cout << "La solution X de AX = B est : X =" << endl << solution << endl;

}

